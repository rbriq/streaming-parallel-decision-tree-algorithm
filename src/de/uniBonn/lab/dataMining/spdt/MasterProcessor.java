package de.uniBonn.lab.dataMining.spdt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.uniBonn.lab.dataMining.spdt.compositeFields.AttributeLeaf;
import de.uniBonn.lab.dataMining.spdt.compositeFields.AttributeLeafClass;
import de.uniBonn.lab.dataMining.spdt.histogram.Histogram;
import de.uniBonn.lab.dataMining.spdt.tree.TreeNode.DecisionTree;

/**
 * The master node responsible for merging all the individual histogram from 
 * the worker nodes
 * @author Rania
 *
 */
public class MasterProcessor {
	
	/**
	 * the accumulated histograms at the master node, which is built
	 * from merging histograms from several worker nodes (
	 * based on leaf, attribute, AND label).
	 */
	private  Map<AttributeLeafClass, Histogram> attLeafClassHistMap = new HashMap<AttributeLeafClass, Histogram>();
	/**
	 * the number of candidates 
	 */
	private int candidatesNum = 20; // B~
	/**
	 * the histograms merged based on leaf and attribute only
	 * (excluding the label)
	 */
	private Map<AttributeLeaf, Histogram> attLeafHistMap = new HashMap<AttributeLeaf, Histogram>();;
	
	private DecisionTree tree = new DecisionTree(candidatesNum);
	
	private List<WorkerProcessor> workerProcessors = new ArrayList<WorkerProcessor>();
	
	/**
	 * a new histogram has been communicated from one
	 * of the worker nodes
	 * @param histogram
	 */
	public void receiveHistogram(Map<AttributeLeafClass, Histogram> newHistograms) {
		mergeHistograms(newHistograms);
	}
	
	public void mergeHistograms(Map<AttributeLeafClass, Histogram> newHistograms) {
		for(AttributeLeafClass curKey : newHistograms.keySet()) {
			// first merge based on leaf, attribute and class 
			if(attLeafClassHistMap.containsKey(curKey)) {
				attLeafClassHistMap.get(curKey).mergeHistogram(newHistograms.get(curKey));
			}
			else {
				Histogram newHist = new Histogram(newHistograms.get(curKey));
				attLeafClassHistMap.put(curKey, newHist);
				//System.out.println(attLeafClassHistMap.get(curKey));
			}
			// now merge by the leaf and attribute without the class
			AttributeLeaf curAttLeaf = curKey.getAttLeaf();
			if(attLeafHistMap.containsKey(curAttLeaf)) {
				attLeafHistMap.get(curAttLeaf).mergeHistogram(newHistograms.get(curKey));
			}
			else {
				Histogram newHist = new Histogram(newHistograms.get(curKey));
				attLeafHistMap.put(curAttLeaf, newHist);
				//System.out.println(attLeafHistMap.get(curAttLeaf));
			}
			
		}
	}
	
	/**
	 * this method is called to indicate that all the data batches in the current iteration have been processed and 
	 * the tree can now be created/updated
	 */
	public void onFinishMerge() {
			tree.updateTree(attLeafClassHistMap, attLeafHistMap);
			clearData();
	}
	
	// clear the data of the previous batch for a new batch
	public void clearData() {
		attLeafClassHistMap.clear();
		attLeafHistMap.clear();
	}
	
	/**
	 * set the possible values of attributes and classes from the input data
	 */
	public void setClassesAtts(Collection <String> classes, Collection <String> attributes) {
		tree.setAttributes(attributes);
		tree.setClasses(classes);
	}

	public DecisionTree getTree() {
		return tree;
	}
	public void addWorker(WorkerProcessor worker) {
		workerProcessors.add(worker);
	}

}
