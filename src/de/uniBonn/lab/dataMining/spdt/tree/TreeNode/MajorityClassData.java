package de.uniBonn.lab.dataMining.spdt.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;

class MajorityClassData {

	private List<Double> labelsCount = new ArrayList<Double>();
	private boolean isPerfectSplit = false;
	private String majorityClass = "";
	/**
	 * the count of the majority class at the node 
	 */
	private double maxCount;

	public MajorityClassData() {

	}

	public MajorityClassData(List<Double> labelsCount, boolean isPerfect,
			String majorityClass) {
		this.labelsCount = labelsCount;
		this.isPerfectSplit = isPerfect;
		this.majorityClass = majorityClass;
	}

	public List<Double> getLabelsCount() {
		return labelsCount;
	}

	public void setLabelsCount(List<Double> labelsCount) {
		this.labelsCount = labelsCount;
	}

	public boolean isPerfectSplit() {
		return isPerfectSplit;
	}

	public void setPerefect(boolean isPerfectSplit) {
		this.isPerfectSplit = isPerfectSplit;
	}

	public String getMajorityClass() {
		return majorityClass;
	}

	public void setMajorityClass(String majorityClass) {
		this.majorityClass = majorityClass;
	}

	public void setMaxCount(double maxCount) {
		this.maxCount = maxCount;
		
	}
	
	public double getMaxCount() {
		return maxCount;
		
	}

}
