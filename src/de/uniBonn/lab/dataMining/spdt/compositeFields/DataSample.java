package de.uniBonn.lab.dataMining.spdt.compositeFields;

import java.util.HashMap;
import java.util.Map;

/**
 * this class represents a data sample, contains a list of att-value pairs and a
 * class value associated with the the sample
 * 
 * @author Rania
 *
 */
public class DataSample {

	/**
	 * a list of attribute
	 */
	private Map<String, Double> attValueMap;

	/**
	 * the class label associated with this data sample
	 */

	private String classLabel;

	public DataSample() {
		attValueMap = new HashMap<String, Double>();
	}

	public void addAttValue(String att, double value) {
		attValueMap.put(att, value);
	}

	public Map<String, Double> getAttValueMap() {
		return attValueMap;
	}

	public String getClassLabel() {
		return classLabel;
	}

	public void setClassLabel(String classLabel) {
		this.classLabel = classLabel;
	}

}
