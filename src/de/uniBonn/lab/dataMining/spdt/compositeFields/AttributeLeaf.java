package de.uniBonn.lab.dataMining.spdt.compositeFields;

import de.uniBonn.lab.dataMining.spdt.tree.TreeNode.TreeNode;

/**
 * a class that represents an attribute and a leaf (used as a key in the histograms
 * maps that are created based on a leaf and attribute: h(v,i)) 
 * @author Rania
 *
 */
public class AttributeLeaf {
	
	private TreeNode leaf;
	private String attribute;
	
		
	public AttributeLeaf() {
	}
	
	public AttributeLeaf(TreeNode leaf, String attribute) {
		this.leaf = leaf;
		this.attribute = attribute;
	}
	public TreeNode getLeaf() {
		return leaf;
	}

	public void setLeaf(TreeNode leaf) {
		this.leaf = leaf;
	}

	public String getAtttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	
	
	@Override
	public boolean equals(Object other) {
		if(!(other instanceof AttributeLeaf))
			return false;
		AttributeLeaf otherAttValClass = (AttributeLeaf)other;
	    return (otherAttValClass.getLeaf().equals(this.leaf) && otherAttValClass.getAtttribute().equals(this.attribute));
	}
	
	@Override
	public int hashCode() {
		int hash = 1;
		hash = hash * 31 + leaf.hashCode(); 
	    hash = hash * 31 + attribute.hashCode();
	    return hash;
	}

}
